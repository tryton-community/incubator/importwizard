# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

import csv
import io

from . import Reader

FILETYPE = "CSV"


class Reader(Reader):

    def __init__(self, data, encoding='utf-8'):
        iterable = io.BytesIO(data)
        iterable = io.TextIOWrapper(iterable, encoding=encoding)
        self._reader = csv.reader(iterable)

    def sheetnames(self):
        return []

    def sheet(self, name):
        assert name in (None, '', 0)  # TODO raise internal error
        return self._reader

    def _rows(self, sheet):
        return sheet
