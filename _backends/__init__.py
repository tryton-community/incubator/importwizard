# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
import logging

from trytond.exceptions import UserError
from trytond.i18n import gettext

__all__ = ['backends']

# Terminate when so many empty rows after each other occurred.
# This is primary to work around LibreOffice not restricting the
# number of rows.
MAX_NUM_CONSECUTIVE_EMPTY_ROWS = 100

logger = logging.getLogger(__name__.rsplit('.', 1)[0])


class DependencyMissing(ModuleNotFoundError): pass  # noqa: E701
class SheetnameRequired(ValueError): pass  # noqa: E701


NoSheetsInFile = UserError(gettext(
    'importwizard.msg_data_import_no_sheets_in_file'))


class Reader:
    def rows(self, sheetname=None,
             get_empty_rows=True,
             max_num_consecutive_empty_rows=MAX_NUM_CONSECUTIVE_EMPTY_ROWS):
        sheet = self.sheet(sheetname)  # may raise SheetnameRequired
        empty_rows_count = 0
        for row in self._rows(sheet):
            assert isinstance(row, (tuple, list))
            # strip trailing empty cells  FIXME rethink
            i = len(row) - 1
            while i >= 0 and row[i] in (None, ''):
                i -= 1
            row = row[:i + 1]
            if row:
                empty_rows_count = 0
                yield row
            else:
                empty_rows_count += 1
                if get_empty_rows:
                    yield row
                if empty_rows_count >= max_num_consecutive_empty_rows:
                    break


backends = {}

for name in ['csv', 'xlsx', 'ods']:
    try:
        mod = importlib.import_module(f'.{name}', __name__)
        backends[name] = (mod.Reader, mod.FILETYPE)
        logger.info("Import-Wizard backend %s installed", name.upper())
    except DependencyMissing:  # pragma: no cover
        logger.warning(
            "Backend module for Import-Wizard backend %s is not "
            "installed or failed to import. Wizards based on the "
            "ImportWizards module will not be able to use this format.",
            name.upper())
