# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

import io

from . import DependencyMissing, NoSheetsInFile, Reader, SheetnameRequired

try:
    from openpyxl import load_workbook
except ImportError:  # pragma: no cover
    raise DependencyMissing('openpyxl') from None


FILETYPE = "Excel (xlsx)"


class Reader(Reader):

    def __init__(self, data):
        data = io.BytesIO(data)
        self._workbook = load_workbook(
            filename=data, read_only=True, data_only=True)

    def sheetnames(self):
        if not self._workbook.worksheets:
            raise NoSheetsInFile
        return self._workbook.sheetnames

    def sheet(self, name):
        if not self._workbook.worksheets:
            raise NoSheetsInFile
        if name is None:
            raise SheetnameRequired(self.sheetnames())
        return self._workbook[name]

    def _rows(self, sheet):
        return sheet.iter_rows(values_only=True)
