# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from . import DependencyMissing, NoSheetsInFile, Reader, SheetnameRequired

try:
    raise ImportError
except ImportError:
    raise DependencyMissing('ods-reader') from None


FILETYPE = "Opendocument Sheet (ods)"


class Reader(Reader):

    def __init__(self, data):
        raise NotImplementedError
        self._workbook = None

    def sheetnames(self):
        if not self._workbook.worksheets:
            raise NoSheetsInFile
        return [str(s) for s in self._workbook.worksheets]

    def sheet(self, name):
        if not self._workbook.worksheets:
            raise NoSheetsInFile
        if name is None:
            raise SheetnameRequired(self.sheetnames())
        return self._workbook[name]

    def _rows(self, sheet):
        return sheet.iter_rows(values_only=True)
