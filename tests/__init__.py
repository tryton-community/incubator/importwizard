# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from .test_csv import CSV_DATA
from .test_csv import RESULT_ROWS as CSV_RESULT_ROWS
from .test_xlsx import RESULT_ROWS as XLSX_RESULT_ROWS
from .test_xlsx import XLSX_DATA

__all__ = ('CSV_DATA', 'CSV_RESULT_ROWS',
           'XLSX_DATA', 'XLSX_RESULT_ROWS')
