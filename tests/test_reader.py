# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import TestCase

from trytond.modules.importwizard import _backends as backends


class TestReader(TestCase):

    def test_skip_many_empty_rows(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=False,
                                max_num_consecutive_empty_rows=100))
        self.assertSequenceEqual(rows, [['aaa']])

    def test_get_many_empty_rows(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=True,
                                max_num_consecutive_empty_rows=100))
        self.assertSequenceEqual(rows, [[]] * 20 + [['aaa']])

    def test_terminate_on_consecutive_empty_rows__skip_empty(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=False,
                                max_num_consecutive_empty_rows=10))
        self.assertFalse(rows)

    def test_terminate_on_consecutive_empty_rows__get_empty(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=True,
                                max_num_consecutive_empty_rows=10))
        self.assertSequenceEqual(rows, [[]] * 10)

    def test_terminate_on_first_empty_rows__skip_empty(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=False,
                                max_num_consecutive_empty_rows=1))
        self.assertFalse(rows)

    def test_terminate_on_first_empty_rows__get_empty(self):
        reader, filetype = backends.backends['csv']
        file_data = b'\n' * 20 + b'aaa'
        reader = reader(file_data)
        rows = list(reader.rows(get_empty_rows=True,
                                max_num_consecutive_empty_rows=1))
        self.assertSequenceEqual(rows, [[]])
