.. This file is part of trytoncommunity-importwizard.
   Licensed under the GNU General Public License v3 or later (GPLv3+).
   The COPYRIGHT file at the top level of this repository contains the
   full copyright notices and license terms.
   SPDX-License-Identifier: GPL-3.0-or-later

=====================
Importwizard Scenario
=====================

Imports::

  >>> from proteus import Model, Wizard, Report
  >>> from trytond.tests.tools import activate_modules
  >>> from trytond.pool import Pool
  >>> import trytond.modules.importwizard as IW
  >>> import trytond.modules.importwizard.tests

Hook in a wizard for testing import::

  >>> COLLECTED_ROWS = []
  >>> class ImportTester(IW.ImportWizard()):
  ...     '''Import Wizard for Testing'''
  ...     __name__ = 'importwizard_tests.import.test'
  ...     def process_rows(self, rows):
  ...         global COLLECTED_ROWS
  ...         COLLECTED_ROWS.clear()
  ...         COLLECTED_ROWS.extend(rows)
  ...         self.result.num_datasets = len(COLLECTED_ROWS)
  ...
  >>> IW.__ImportTester = ImportTester

Need to disable DB_CACHE to allow hooking in the wizard for testing
import::

  >>> orig_DB_CACHE = trytond.tests.test_tryton.DB_CACHE
  >>> trytond.tests.test_tryton.DB_CACHE = None

Activate modules::

  >>> config = activate_modules('importwizard')


Import an empty CSV file
========================

::

   >>> importer = Wizard('importwizard_tests.import.test')
   >>> importer.form.file_ = b''
   >>> importer.form.file_format = 'csv'
   >>> importer.execute('import_')
   >>> importer.form_state
   'result'
   >>> importer.form.num_datasets
   0
   >>> importer.execute('end')
   >>> importer.state
   'end'
   >>> COLLECTED_ROWS
   []

Import a CSV file with content
==============================

::

   >>> importer = Wizard('importwizard_tests.import.test')
   >>> importer.form.file_ = IW.tests.CSV_DATA
   >>> importer.form.file_format = 'csv'
   >>> importer.execute('import_')
   >>> importer.form_state
   'result'
   >>> importer.form.num_datasets == len(IW.tests.CSV_RESULT_ROWS)
   True
   >>> importer.execute('end')
   >>> importer.state
   'end'
   >>> COLLECTED_ROWS == IW.tests.CSV_RESULT_ROWS
   True


Import a XLSX file with content
===============================

::

   >>> importer = Wizard('importwizard_tests.import.test')
   >>> importer.form.file_ = IW.tests.XLSX_DATA
   >>> importer.form.file_format = 'xlsx'
   >>> importer.form.sheet  # FIXME: get from data
   'Sheet'
   >>> importer.execute('import_')
   >>> importer.form_state
   'result'
   >>> importer.form.num_datasets == len(IW.tests.XLSX_RESULT_ROWS)
   True
   >>> importer.execute('end')
   >>> importer.state
   'end'
   >>> COLLECTED_ROWS == IW.tests.XLSX_RESULT_ROWS
   True


Cleanup
===========

Restore DB_CACHE for further tests::

   >>> trytond.tests.test_tryton.DB_CACHE = orig_DB_CACHE
