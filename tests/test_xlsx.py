# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from tempfile import NamedTemporaryFile
from unittest import TestCase

from openpyxl import Workbook

from trytond.exceptions import UserError
from trytond.modules.importwizard import _backends as backends

XLSX_ROWS = [
    [1, 2, 3, '2023-12-23', 4, ''],
    ['aa', '1', 0],
    [],
    ['', '', '', '', '', 'xxx', None, None, None],
]

RESULT_ROWS = [
    (1, 2, 3, '2023-12-23', 4),
    ('aa', '1', 0),
    (),
    (None, None, None, None, None, 'xxx'),
]


def _generate_workbook(rows):
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    for row in rows:
        ws.append(row)
    with NamedTemporaryFile() as tmp:
        wb.save(tmp.name)
        tmp.seek(0)
        return tmp.read()


XLSX_DATA = _generate_workbook(XLSX_ROWS)


class TestXlsxBackend(TestCase):

    def test_classes(self):
        self.assertIn('xlsx', backends.backends)
        self.assertEqual(len(backends.backends['xlsx']), 2)
        reader, filetype = backends.backends['xlsx']
        assert issubclass(reader, backends.Reader)
        self.assertIsInstance(filetype, str)

    def test_simple(self):
        reader, filetype = backends.backends['xlsx']
        file_data = XLSX_DATA
        reader = reader(file_data)
        sheetnames = reader.sheetnames()
        self.assertTrue(sheetnames)
        rows = list(reader.rows(sheetname=sheetnames[0]))
        self.assertSequenceEqual(rows, RESULT_ROWS)

    def test_sheetnames__no_sheet_in_file(self):
        reader, filetype = backends.backends['xlsx']
        file_data = XLSX_DATA
        reader = reader(file_data)
        del reader._workbook[reader._workbook.sheetnames[0]]
        self.assertRaisesRegex(UserError, 'no_sheets_in_file',
                               reader.sheetnames)

    def test_sheet__no_sheet_in_file(self):
        reader, filetype = backends.backends['xlsx']
        file_data = XLSX_DATA
        reader = reader(file_data)
        del reader._workbook[reader._workbook.sheetnames[0]]
        self.assertRaisesRegex(UserError, 'no_sheets_in_file',
                          reader.sheet, 'Non existing Sheet')

    def test_sheet__no_sheetname_given(self):
        reader, filetype = backends.backends['xlsx']
        file_data = XLSX_DATA
        reader = reader(file_data)
        self.assertRaises(backends.SheetnameRequired, reader.sheet, None)
