# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import TestCase

from trytond.modules.importwizard import _backends as backends

CSV_DATA = b"""\
1,2,3,4
"aa","1"

,,,,,xxx,,,
"""

RESULT_ROWS = [
    ['1', '2', '3', '4'],
    ['aa', '1'],
    [],
    ['', '', '', '', '', 'xxx'],
]


class TestCSVBackend(TestCase):

    def test_classes(self):
        self.assertIn('csv', backends.backends)
        self.assertEqual(len(backends.backends['csv']), 2)
        reader, filetype = backends.backends['csv']
        assert issubclass(reader, backends.Reader)
        self.assertIsInstance(filetype, str)

    def test_simple(self):
        reader, filetype = backends.backends['csv']
        file_data = CSV_DATA
        reader = reader(file_data)
        sheetnames = reader.sheetnames()
        self.assertFalse(sheetnames)
        rows = list(reader.rows())
        self.assertSequenceEqual(rows, RESULT_ROWS)
