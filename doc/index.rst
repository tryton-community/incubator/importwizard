.. This file is part of trytoncommunity-importwizard.
   Licensed under the GNU Free Documentation License v1.3 or any later version.
   The COPYRIGHT file at the top level of this repository contains the
   full copyright notices and license terms.
   SPDX-License-Identifier: GFDL-1.3-or-later

###################
ImportWizard Module
###################

This module provides a common base for Tryton import wizards for
tables.
It provides a start view to select the file, reads the file
and passes an row iterator to a method
(which needs to be overwritten by the concrete importer).
When done, it shows the number of imported modules.

The idea is to get boilerplate away from the developers.

When installing just this module, it is not available to the user.
Developers need to implement a specific wizard on top of this module's classes.
Please see trytoncommunity-importwizard-examples for some examples.

Installation
---------------

To install only CSV support::

  pip install trytoncommunity-importwizard

To install CSV and XLSX (Excel) support::

  pip install trytoncommunity-importwizard[xlsx]


.. toctree::
   :maxdepth: 2

   setup
   usage
   configuration
   design
   reference
   releases
