# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.pool import Pool

from . import importwizard
from .importwizard import ImportWizard, ImportWizardResult, ImportWizardStart

__all__ = ['ImportWizard', 'ImportWizardStart', 'ImportWizardResult']

# private, used for tests
__ImportTester = None


def register():
    Pool.register(
        importwizard.ImportWizardStart,
        importwizard.ImportWizardResult,
        module='importwizard', type_='model')
    if __ImportTester:
        Pool.register(
            __ImportTester,
            module='importwizard', type_='wizard')
