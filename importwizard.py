# This file is part of trytoncommunity-importwizard.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.model import ModelView, fields
from trytond.wizard import Button, StateTransition, StateView, Wizard

from . import _backends


class DataError(Exception):
    pass


class ImportWizardStart(ModelView):
    "Wizard Import Start"
    __name__ = 'importwizard.import.start'

    AVAILABE_BACKENDS = [
        (name, be[1]) for name, be in _backends.backends.items()
    ]

    @classmethod
    @property
    def AVAILABE_BACKEND_CODES(cls):
        return set(be[0] for be in cls.AVAILABE_BACKENDS)

    file_ = fields.Binary("File", required=True)
    file_format = fields.Selection(AVAILABE_BACKENDS,
        "File Format", required=True, translate=False)
    sheet = fields.Selection('get_sheetnames', "Sheet to import",
                             translate=False)

    @classmethod
    def default_file_format(cls):
        return 'csv'

    def get_row_reader(self):
        try:
            rr_cls = _backends.backends[self.file_format][0]
        except KeyError as exc:  # pragma: no cover
            raise Exception("Internal Error: Import backend %r requested "
                            "but is not available" % exc.args[0])
        return rr_cls(self.file_)

    @fields.depends('file_', 'file_format')
    def get_sheetnames(self):
        if not (self.file_ and self.file_format):
            return ()
        row_reader = self.get_row_reader()
        sheetnames = row_reader.sheetnames() or []
        return [(n, n) for n in sheetnames]

    @fields.depends(methods=['get_sheetnames'])
    def on_change_with_sheet(self):
        sheetnames = self.get_sheetnames()
        return sheetnames[0][0] if sheetnames else None


class ImportWizardResult(ModelView):
    "Wizard Import Result"
    __name__ = 'importwizard.import.result'

    num_datasets = fields.Integer("Datasets")


def ImportWizard(
        start_model='importwizard.import.start',
        start_view='importwizard.importwizard_start_form',
        result_model='importwizard.import.result',
        result_view='importwizard.importwizard_result_form'):

    class ImportWizard(Wizard):

        max_num_consecutive_empty_rows = 100

        start = StateView(
            start_model, start_view, [
                Button("Cancel", 'end', 'tryton-cancel'),
                Button("Import", 'import_', 'tryton-ok', default=True),
            ])
        import_ = StateTransition()
        result = StateView(
            result_model, result_view, [
                Button("Close", 'end', 'tryton-close', default=True),
            ])

        def default_result(self, fields):
            return {
                'num_datasets': getattr(self.result, 'num_datasets', -42)
            }

        def transition_import_(self):
            self.row_reader = self.start.get_row_reader()
            sheetname = getattr(self.start, 'sheet', None)
            # get_empty_row=True to get correct row number
            rows = self.row_reader.rows(
                sheetname=sheetname, get_empty_rows=True,
                max_num_consecutive_empty_rows=self.max_num_consecutive_empty_rows)  # noqa: E501
            self.process_rows(rows)
            return 'result'

        def process_rows(self, rows):  # pragma: no cover
            """Abstract Method, overwrite in your implementation"""
            self.result.num_datasets = 0
            raise NotImplementedError

    return ImportWizard
